from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Yunie Debora' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 6, 20) #TODO Implement this, format (Year, Month, Date)
npm = 1806147224 # TODO Implement this
contact = 'yuniedebora20@gmail.com'
ukm = 'PSM UI Paragita'
hobi = 'menyanyi'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'kontak' : contact, \
                'age': calculate_age(birth_date.year), 'npm': npm, \
                'status': status(birth_date.year), 'ukm' : ukm,\
                'hobi': hobi}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

def status (birth_date):
    age = calculate_age(birth_date)
    return 'sedang berkuliah di Universitas Indonesia' if (18 <= age <= 21) else 'lulusan dari Universitas Indonesia'
    
